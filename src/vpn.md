# VPNs

A VPN or Virtual Private Network is a tunnel to route your internet service through another device and can be useful for concealing your web traffic from your ISP (internet service provider), bypassing regional restrictions and hiding your IP adress from websites.

In the past few years, VPN usage have greatly increased in popularity and have become largely commercialized. Though, many VPN companies will intentionally mislead you to buy their product and there are still many [misconceptions](#Common_Misconceptions) regarding what your ISP can see and how much VPNs truly protect you.

Remember that a VPN can see anything your existing internet service provider can and when you're putting that information in the hands of a private company, watch out for dangerous factors like logging and data selling.

If your [threat model](./threat_model.md) is to stay anonymous, [Tor](https://torproject.org) is recommended over a traditional VPN. Tor, or The Onion Router is a protocol to bypass censorship and become anonymous on the web. Tor routes your internet service through several "nodes" and therefore can be quite slow. Tor gets it's name and logo because traffic is hidden under several layers of protection, similar to the layers of an onion. Read more about the Tor Browser and other privacy browsers [here](./browsers.md).

## Recommendations

### 1. Mullvad
<img src="./images/mullvad.svg" width=100>

Mullvad requires no information to start using their VPN service other than a form of payment and they accept private payment methods like cash, Bitcoin and Bitcoin Cash. After placing a payment, you will receive a single account number which you can use to access the VPN. They maintain a strict no-logs policy and reside in Sweden, a country with [strong privacy laws](https://en.wikipedia.org/wiki/Data_Act_(Sweden)) like Switzerland. Without collecting user information or taking logs, the common dangers of leaking are almost entirely nonexistant.

### 2. ProtonVPN
<img src="./images/protonvpn.svg" width=100>

ProtonVPN is run by the same team behind ProtonMail and is based inside Switzerland, a country known for arguably the best data privacy laws in the world. ProtonVPN suits the majority of VPN users as it's free and does require payment details to start using though some may find this aspect untrustworthy as free products must be making a profit from something else, if it's not your money. Although they don't offer Wireguard like Mullvad and IVPN do, they do provide Tor over VPN tunnels and a multi-connection feature called Secure Core.

### 3. IVPN
<img src="./images/ivpn.svg" width=100>

IVPN is a member of the Electronic Frontier Foundation, a nonprofit that fights for digital privacy and freedom. Like Mullvad and ProtonVPN, IVPN has it's app available on F-Droid (the freedom app store for Android) and has open sourced it's mobile and desktop applications. IVPN has an extensive array of servers and provides a multi-connection service like ProtonVPN called multihop to further obscure your web traffic.

## Comparison

|    Name   | Logs |   Country   | Cheapest Tier | VPN Tunnel | Accepts Bitcoin | Kill Switch |
|:---------:|:----:|:-----------:|:-------------:|:----------:|:---------------:|:-----------:|
|  Mullvad  |   ✖  |    Sweden   |     5€/mo.    |  Wireguard |        ✔        |      ✔      |
| ProtonVPN |   ✖  | Switzerland |     0€/mo.    |   OpenVPN  |        ✔        |      ✔      |
|    IVPN   |   ✖  |  Gibraltar  |     $6/mo.    |  Wireguard |        ✔        |      ✔      |

*Mullvad can be reduced to 4.50€/month by paying with Bitcoin or Bitcoin Cash

## Common Misconceptions

1. VPNs make you anonymous on the web.

To websites, you will merely have a different IP address but unless you change your browser, you will still be fingerprintable (identifyable) by the same small details in your browser like it's resolution, canvas fingerprint and user agent.

2. There is a "best" VPN.

Different VPNs will suit different people better based on their [threat model](./threat_model.md), budget and how much privacy they require.

3. VPNs allow you to browse your accounts privately.

If you sign into a website while in a VPN or Tor, you are forfeighting your anonymity and linking that browser session with personally indentifiable information stored in your account.