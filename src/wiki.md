# The Obelisk Wiki

> Short and sweet set of tools to protect your privacy.

Mass surveilance is near inescapable in our digital world. Finding a social media app that doesn't spy on you is a challenge and escaping the watchful eyes of governments and big tech is difficult but possible.

**The Obelisk Wiki** is a hobby project of mine to spread privacy and foster a community of technology that respects it's users. This website is generated from Markdown files using a tool, [mdbook](https://github.com/rust-lang/mdBook), to provide beautiful and minimal docs and information.

Use the sidebar on the left to quickly jump between sections or use the big arrows on the left and right to navigate across topics linearly.

## Ideals

What are some ideals that The Obelisk Wiki looks for in the applications it recommends?

### Open Source

Transparency is key to understanding how a product works and treats the privacy of it's users. Open source software means the code is publicly accessible online to be audited for good practices and improved upon by users. The beauty of open source is the ability to modify and improve upon the technology you use, to make your life easier and contribute to someone else's project.

This wiki will only advise open source software and will use terms like FOSS (free and open source software) or FLOSS (free libre open source software). 

### Freedom

You should have complete control over the software you use. Freedom ties in very closely with open source but can be interpreted differently. The term "libre" is often used to describe software that respects the freedom of it's users. Libre software will provide customization to it's users and value inclusivity over exclusivity.

### Frugality

Hardening your life shouldn't break the bank and this guide is designed so that you won't have to. Each tool here is thoroughly researched and I gain no money from any of these suggestions. The Obelisk Wiki is structured to be in the user's best interest.